tsdecrypt (10.0-4) unstable; urgency=medium

  * Team upload
  * debian/patches: Fix missing #includes.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 10 Mar 2024 19:25:28 +0100

tsdecrypt (10.0-3) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

 -- Bastian Germann <bage@debian.org>  Sun, 24 Sep 2023 16:06:33 +0200

tsdecrypt (10.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Reiner Herrmann ]
  * strip build date to enable reproducible building (Closes: #829713)

  [ Vagrant Cascadian ]
  * debian/rules: Pass default CFLAGS. (Closes: #1022130)

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Thu, 10 Nov 2022 13:54:44 -0800

tsdecrypt (10.0-2) unstable; urgency=low

  [ Peter Michael Green ]
  * Remove inappropriate compiler flags. (Closes: #730415)
  * Fix clean target.

  [ Alessio Treglia ]
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Mon, 24 Feb 2014 14:52:09 +0000

tsdecrypt (10.0-1) unstable; urgency=low

  * New upstream release:
    - Add --ecm-only (-v) option. This allows processing of ECMs but without
      decoding the input stream.
    - Add --ecm-and-emm-only (-q) option. This allows processing of ECMs
      and EMMs but without decoding the input stream.
    - Set newcamd client id to 0x7878 (tsdecrypt).
    - Fixed regression that apeared in 9.0 which prevented the output from
      working if the output address was not multicast.
    - Add support for Griffin CAS and for DGCrypt CAS.
    - Add support for setting multicast source address /SSM/ (--input-source).
    - Add support for EMM filters similar to DVBAPI filters (match + mask).
    - Add support for EMM filters based on section length.
    - Add support for Irdeto CHID filtering (tested with Raduga).
  * Statically set DESTDIR and PREFIX in debian/rules to fix prevent build
    failures (Closes: #713528). Fix path creation too.
  * Use canonical form for VCS urls.
  * Update copyright holder info for Debian packaging, remove Comment field
    from License stanzas.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Thu, 03 Oct 2013 15:50:12 +0100

tsdecrypt (9.0-1) unstable; urgency=low

  * New upstream release.

 -- Alessio Treglia <alessio@debian.org>  Fri, 12 Oct 2012 01:08:09 +0100

tsdecrypt (8.1-1) unstable; urgency=low

  * New upstream release.
  * Remove all patches, applied upstream.
  * Explicitly build with "make dvbcsa" when building with libdvbcsa.
  * debian/copyright:
    - Some code are GPL-2 only, not GPL-2+.
    - Fix reference to GPL-2 license file.

 -- Alessio Treglia <alessio@debian.org>  Mon, 30 Apr 2012 02:47:10 +0200

tsdecrypt (8.0-2) unstable; urgency=low

  * Make 'postrm remove' and 'postrm disappear' remove the
    /usr/bin/tsdecrypt alternative (Closes: #668971).
  * Don't remove the alternative on 'prerm deconfigure' to
    preserve user configuration.

 -- Alessio Treglia <alessio@debian.org>  Mon, 16 Apr 2012 18:46:50 +0200

tsdecrypt (8.0-1) unstable; urgency=low

  * New upstream release:
    - Add --biss-key (-Q) option. This option enables BISS decryption.
    - Add FFdecsa support. In most cases FFdecsa decrypts faster than
      libdvcsa.
    - Add --const-cw (-Y) option. This option allows using of constant
      code word for decryption.
    - Display better timing information for ECMs and code word changes.
    - Filter special symbols in provider and service names before
      printing them.
  * debian/patches/1001-dont_strip_binaries.patch:
    - Don't install stripped binaries.
  * debian/patches/1002-bashisms.patch:
    - Fix bashism to prevent FTBFS.
  * Refresh other patches.
  * Compile tsdecrypt twice, one per library:
    - tsdecrypt_dvbcsa uses libdvbcsa's decrypting routines
    - tsdecrypt_ffdecsa uses FFdecsa's decrypting routines
  * Install alternatives to let users choose which CSA decrypting
    library is appropriate for them.
  * Install manpages.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Sun, 15 Apr 2012 12:54:23 +0200

tsdecrypt (7.0-1) unstable; urgency=low

  * Initial release. (Closes: #667461)

 -- Alessio Treglia <alessio@debian.org>  Wed, 04 Apr 2012 09:42:43 +0200
